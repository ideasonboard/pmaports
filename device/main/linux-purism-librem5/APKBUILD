# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Alistair Francis <alistair@alistair23.me>
# Co-Maintainer: Newbyte <newbyte@postmarketos.org>
pkgname=linux-purism-librem5
pkgver=6.1.12
pkgrel=0
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="
	bash
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
install="$pkgname.post-upgrade"

# Source
_repository="linux"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"


source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/linux/-/archive/pureos/$_purismver/linux-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare

	# NOTE: only for generating a config based on Purism's defconfig, for rebasing...
	# make ARCH="$_carch" CC="${CC:-gcc}" \
	# 	defconfig KBUILD_DEFCONFIG=librem5_defconfig
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"

}

sha512sums="
98089c72b0fefaf201cbeae5e60da67d4f0382a8caac6b07745cabd644d3455e71da3830144d3a2fdc78799ef2f20a483355c5a765944f8c92c74565d22ad032  linux-purism-librem5-6.1.12pureos1.tar.gz
934e9973fa92b6138acd8a7cba95f8a174d1634bf4530d10cc410debf98f59e19b857fdc297661f496b377fa7b5b5b820bf6f4c2428c9c5b53a8f5b60e82a8e5  config-purism-librem5.aarch64
"
